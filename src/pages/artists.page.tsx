import React, { useEffect, useState } from 'react';
import { NextPage, NextPageContext } from 'next';
import { ArtistsService } from '../../src/common/services/artists.service';
import { Artist } from '../common/interfaces/artist.interface';
import ArtistList from '../components/artist-list.component';
import { Pagination } from 'antd';
import Search from 'antd/lib/input/Search';
import Space from '../components/space.component';

interface IProps {
  artists: Artist[],
  artistCount: number
}

const sizePage = 24;

const ArtistsPage: NextPage<IProps> = (props: IProps) => {

  const artistsService = ArtistsService.getInstance();

  const [artists, setArtists] = useState<Artist[]>(props.artists);
  const [search, setSearch] = useState<string>('');
  const [pageNumber, setPageNumber] = useState<number>(1);

  useEffect(() => {
    setArtists(props.artists)
  }, props.artists);

  async function onChange(pageNumber) {
    setArtists(await artistsService.getAllByPage(pageNumber - 1));
    setPageNumber(pageNumber)
  }

  async function onSearch(value: string) {
    const spaceInSearch = value.split(' ').length > 1;
    const [firstname, lastname] = spaceInSearch
      ? value.split(' ')
      : [value, value]

    const artists = value
      ? await artistsService.getByFirstnameAndLastname(firstname, lastname)
      : await artistsService.getAllByPage(pageNumber - 1)

    setArtists(artists);      
    setSearch(value);
  }
  

  return (
    <div>
      <div className="page-title-block">
        <h1>Artistes</h1>
        <Search
          placeholder="Rechercher des artistes (prénom complet, nom complet, prénom complet nom complet)"
          allowClear  
          onSearch={onSearch} 
          style={{ width: 800, margin: '0 10px' }}
        />
      </div>

      <Space height={64} />

      <ArtistList artists={artists} />

      <Space height={64} />

      <div className="pagination-line">
        <Pagination
          defaultCurrent={1}
          pageSize={sizePage}
          total={props.artistCount}
          onChange={onChange}
          disabled={!!search}
          current={pageNumber}
        />
      </div>
    </div>
  )
}

ArtistsPage.getInitialProps = async function(context: NextPageContext) {
  const artistsService = ArtistsService.getInstance();
  const artists = await artistsService.getAll();
  const artistCount = await artistsService.count();
  return { artists, artistCount } as IProps;
}

export default ArtistsPage;


