import React from 'react';
import { User } from '../../src/common/interfaces/user.interface';
import { NextPage, NextPageContext } from 'next';
import { UsersService } from '../../src/common/services/users.service';
import { Form, Input, Button } from 'antd';
import { useRouter } from 'next/router';


const RegistrationPage: NextPage = () => {

  const router = useRouter()
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const usersService = UsersService.getInstance()

  const onFinish = values => {
    usersService.create(values);
    // TODO: Branch to api and redirect to login
    router.push('/');
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <h1>Création de compte</h1>
      <Form
      {...layout}
      name="basic"
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Email"
        name="email"
        rules={[{ required: true, message: 'Rentrez votre email' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Pseudo"
        name="pseudo"
        rules={[{ required: false, message: 'Rentrez votre pseudo' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Image de profil"
        name="pictureURI"
        rules={[{ required: false, message: 'mettez un lien de votre image de profil' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Mot de passe"
        name="password"
        rules={[{ required: true, message: 'Tapez votre mot de passe' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Créer son compte
        </Button>
      </Form.Item>
    </Form>
      
    </div>
  )
}

export default RegistrationPage;


