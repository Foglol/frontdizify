import React from 'react';
import { NextPage } from 'next';
import { AlbumListAdmin } from '../components/admin/album-list-admin.component'

import { Admin, Resource, ListGuesser } from 'react-admin';
import { ArtistListAdmin } from '../components/admin/artist-list-admin.component';

import { dataProvider } from '../common/admin/data.provider';
import { UserLevel } from '../common/enums/user-level.enum';
import { privateRoute } from '../hoc/private-route.hoc';
import { UserListAdmin } from '../components/admin/user-list-admin.component';
import { TitleListAdmin } from '../components/admin/title-list-admin.component';

interface IProps {

}

const AdminPage: NextPage<IProps> = (props: IProps) => {

  // const dataProvider = jsonServerProvider(process.env.NEXT_PUBLIC_API_URL);

  return (
    <div className="admin-page">
      <Admin dataProvider={dataProvider} >
        <Resource name="album" list={AlbumListAdmin} />  
        <Resource name="artist" list={ArtistListAdmin} />
        <Resource name="user" list={UserListAdmin} />
        <Resource name="title" list={TitleListAdmin} />
      </Admin>
    </div>
    
  )
}

export default privateRoute(AdminPage, UserLevel.Admin);


