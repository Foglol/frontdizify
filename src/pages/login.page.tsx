import React from 'react';
import { NextPage, NextPageContext } from 'next';
import { UsersService } from '../../src/common/services/users.service';
import { Form, Input, Button } from 'antd';
import { useRouter } from 'next/router';

const LoginPage: NextPage = () => {

  const router = useRouter()
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
  };

  const usersService = UsersService.getInstance()

  const onFinish = async ({ email, password }) => {
    await usersService.connect({email, password});
    router.push('/');
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <h1>Connexion</h1>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >

          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Rentrez votre email' }]}>
            <Input />
          </Form.Item>

          <Form.Item
            label="Mot de passe"
            name="password"
            rules={[{ required: true, message: 'Tapez votre mot de passe' }]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
            Se connecter
            </Button>
          </Form.Item>
        </Form>
      
    </div>
  )
}

export default LoginPage;


