import { Artist } from './artist.interface';
import { Title } from './title.interface';
import { Album } from './album.interface';

export interface Favorite {
  id: number;
  albums?: Album[];
  artists?: Artist[];
  titles?: Title[];
}