import { Title } from './title.interface';

export interface Playlist {
  id: number;
  titles: Title[];
}