export interface User {
  id?: number,
  email: string,
  pseudo: string,
  pictureURI: string,
  playlists: [],
  password?: string
  token?: string
}