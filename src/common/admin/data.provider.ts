import { ApiService } from '../services/api.service'
import { DataProvider, GetListParams, GetListResult, GetManyParams, GetManyResult, DeleteManyParams, DeleteManyResult, DeleteParams, DeleteResult, Identifier } from 'react-admin'
import { AuthToken } from '../services/auth-token.service';


const apiService = ApiService.getInstance();

/**
 * Custom data provider for react admin.
 * Work as an adapter to make automatic requests from a resource name string
 */
export const dataProvider: DataProvider = {
  getList: async <RecordType>(resource: string, params: GetListParams): Promise<GetListResult<RecordType>> => {
    const { page, perPage } = params.pagination;
    const { field, order } = params.sort;
    const { token } = await AuthToken.fromNext(null);

    const data = (await apiService.request('get', `${resource}?page=${page - 1}&size=${perPage}`, { headers: { token } })) as RecordType[]
    return { data: data, total: 200 } as GetListResult<RecordType>
  },

  getOne: (resource, params) => Promise.resolve({ data: undefined }),

  getMany: async <RecordType>(resource: string, params: GetManyParams): Promise<GetManyResult<RecordType>> => {
    const { ids } = params;
    const { token } = await AuthToken.fromNext(null);

    const data = await Promise.all(ids.map((id: number) => {
      return apiService.request('get', `${resource}/${id}`, { headers: { token } }) as Promise<RecordType>
    }))

    return { data }
  },

  getManyReference: (resource, params) => Promise.resolve({ data: undefined, total: 0 }),

  delete: async <RecordType>(resource: string, params: DeleteParams): Promise<DeleteResult<RecordType>> => {
    const { id } = params;
    const { token } = await AuthToken.fromNext(null);

    // const data = await apiService.request('delete', `${resource}/${id}`, { headers: { token } }) as RecordType;
    return { data: undefined };
  },

  deleteMany: async (resource: string, params: DeleteManyParams): Promise<DeleteManyResult> => {
    const { ids } = params;
    const { token } = await AuthToken.fromNext(null);

    // const data = await Promise.all(ids.map((id: number) => {
    //   return apiService.request('delete', `${resource}/${id}`, { headers: { token } }) as Promise<Identifier>;
    // }));

    return { data: undefined };
  },

  update: (resource, params) => Promise.resolve({ data: undefined }),

  updateMany: (resource, params) => Promise.resolve({ data: undefined }),

  create: (resource, params) => Promise.resolve({ data: undefined  }),
};
