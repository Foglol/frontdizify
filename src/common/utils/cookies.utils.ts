import { IncomingMessage } from 'http';

/**
 * Get a mapped object of the cookies stored in a request.
 * @param {IncomingMessage} request the request
 * @return {Record<string, string>} the cookies object 
 */
export function getCookies(request: IncomingMessage): Record<string, string> {
  var cookies = {};
  request.headers && request.headers.cookie.split(';').forEach(function(cookie) {
    var parts = cookie.match(/(.*?)=(.*)$/)
    cookies[ parts[1].trim() ] = (parts[2] || '').trim();
  });
  return cookies;
};