import { IBaseCrudService } from './base-crud-service.interface';
import { Artist } from '../interfaces/artist.interface';
import { ArtistsMock } from '../mocks/artists.mock';
import { ApiService } from './api.service';
import { Album } from '../interfaces/album.interface';
import { AlbumsMock } from '../mocks/albums.mock';

export class ArtistsService implements IBaseCrudService<Artist> {

  private static instance: ArtistsService;

  artistsMock = ArtistsMock.getInstance();
  albumsMock = AlbumsMock.getInstance();
  apiService = ApiService.getInstance()

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): ArtistsService {
    if (!ArtistsService.instance) {
      ArtistsService.instance = new ArtistsService();
    }

    return ArtistsService.instance;
  }
  
  count() : Promise<number> {
    return this.apiService.request('get', 'artist/count') as Promise<number>;
  }

  async getAll(): Promise<Artist[]> {
    return this.apiService.request('get', 'artist') as Promise<Artist[]>;
  }

  async getByFirstnameAndLastname(firstname,lastname) : Promise<Artist[]> {
    return this.apiService.request('get', `artist/name/?firstname=${firstname}&lastname=${lastname}`) as Promise<Artist[]>;
  }

  async getTops(): Promise<Artist[]> {
    return this.apiService.request('get', 'artist/top') as Promise<Artist[]>;
  }
  getAllByPage(page: number): Promise<Artist[]> {
    return this.apiService.request('get', `artist/?page=${page}`) as Promise<Artist[]>;
  }

  async getRandoms(): Promise<Artist[]> {
    return this.apiService.request('get', 'artist/random') as Promise<Artist[]>;
  }

  async getAlbums(artistId: number): Promise<Album[]> {
    return this.apiService.request('get', `artist/${artistId}/album/`) as Promise<Album[]>;
  }

  create(object: Artist): Promise<Artist> {
    throw new Error('Method not implemented.');
  }

  async getOne(id: number): Promise<Artist> {
    return this.apiService.request('get', `artist/${id}`) as Promise<Artist>;
  }

  update(id: number, artist: Artist): Promise<Artist> {
    throw new Error('Method not implemented.');
  }

  delete(id: number): Promise<void> {
    throw new Error('Method not implemented.');
  }

}