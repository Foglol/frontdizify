import { Album } from '../interfaces/album.interface';
import { Title } from '../interfaces/title.interface';
import { AlbumsMock } from '../mocks/albums.mock';
import { IBaseCrudService } from './base-crud-service.interface';
import { ApiService } from './api.service';
import { Artist } from '../interfaces/artist.interface';
import { Favorite } from '../interfaces/favorite.interface';

export class FavoritesService implements IBaseCrudService<Album> {
  
  private static instance: FavoritesService;
  private apiService = ApiService.getInstance();

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): FavoritesService {
    if (!FavoritesService.instance) {
        FavoritesService.instance = new FavoritesService();
    }

    return FavoritesService.instance;
  }

  getAll(): Promise<Album[]> {
    // TODO: change this using the ApiService class.
    return this.apiService.request('get', 'album') as Promise<Album[]>;
  }

  async getRandoms(): Promise<Album[]> {
    return this.apiService.request('get', 'album/random') as Promise<Album[]>;
  }

  create(album: Album): Promise<Album> {
    throw new Error('Method not implemented.');
  }

  createArtistFavorite(artist: Artist, token: string): Promise<Favorite> {
    return this.apiService.request('post', `user/favorite/add/artist/${artist.id}`, { headers: { token } }) as Promise<Favorite>;
  }

  createAlbumFavorite(album: Album, token: string): Promise<Favorite> {
    return this.apiService.request('post', `user/favorite/add/album/${album.id}`, { headers: { token } }) as Promise<Favorite>;
  }

  createTitleFavorite(title: Title, token: string): Promise<Favorite> {
    return this.apiService.request('post', `user/favorite/add/title/${title.id}`, { headers: { token } }) as Promise<Favorite>;
  }

  removeArtistFavorite(artist: Artist, token: string): Promise<Favorite> {
    return this.apiService.request('delete', `user/favorite/remove/artist/${artist.id}`, { headers: { token } })as Promise<Favorite>;
  }
  removeAlbumFavorite(album: Album, token: string): Promise<Favorite> {
    return this.apiService.request('delete', `user/favorite/remove/album/${album.id}`, { headers: { token } })as Promise<Favorite>;
  }
  removeTitleFavorite(title: Title, token: string): Promise<Favorite> {
    return this.apiService.request('delete', `user/favorite/remove/title/${title.id}`, { headers: { token } })as Promise<Favorite>;
  }

  getFavoritesForCurrentUser(token: string): Promise<Favorite> {
    return this.apiService.request('get', `user/favorite`, { headers: { token } }) as Promise<Favorite>;
  }

  async getOne(id: number): Promise<Album> {
    // TODO: change this using the ApiService class.
    return this.apiService.request('get', `album/${id}`) as Promise<Album>;
  }

  update(id: number, album: Album): Promise<Album> {
    throw new Error('Method not implemented.');
  }

  delete(id: number): Promise<void> {
    throw new Error('Method not implemented.');
  }
  
}