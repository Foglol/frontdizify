import jwtDecode from "jwt-decode";
import Cookie from "js-cookie";
import { NextPageContext } from 'next';
import { getCookies } from '../utils/cookies.utils';
import { UserLevel } from '../enums/user-level.enum';
import { UsersService } from './users.service';

export type DecodedToken = {
  readonly id: number;
  readonly exp: number;
}

const TOKEN_STORAGE_KEY = "myApp.authToken";

export class AuthToken {

  readonly decodedToken: DecodedToken;
  level: UserLevel;

  constructor(readonly token?: string) {
    // we are going to default to an expired decodedToken
    this.decodedToken = { id: 0, exp: 0 };
    
    // then try and decode the jwt using jwt-decode
    try {
      if (token) {
        this.decodedToken = jwtDecode(token) as DecodedToken;
      }
    } catch (e) {
    }
  }

  public async setup(): Promise<void> {
    await this.setUserLevel(this.token);
  }

  static async storeToken(token: string) {
    Cookie.set(TOKEN_STORAGE_KEY, token);
  }

  private async setUserLevel(token: string) {
    const { admin } = await UsersService.getInstance().checkToken(token);
    this.level = admin ? UserLevel.Admin : UserLevel.User;
  }

  static async removeToken() {
    Cookie.remove(TOKEN_STORAGE_KEY)
  }

  get authorizationString() {
    return `Bearer ${this.token}`;
  }

  get expiresAt(): Date {
    return new Date(this.decodedToken.exp * 1000);
  }

  get isExpired(): boolean {
    return new Date() > this.expiresAt;
  }

  get isValid(): boolean {
    return !this.isExpired;
  }

  /**
   * Retrieve the token from a next page context
   * @param {NextPageContext} context the next page context
   * @return {string} token
   */
  static async fromNext(context: NextPageContext): Promise<AuthToken> {
    let token: string;
    if (!context?.res) { // is loaded on client
      token = Cookie.get(TOKEN_STORAGE_KEY)
    } else { // is loaded on server
      try {
        const cookies = getCookies(context.req);
        token = cookies[TOKEN_STORAGE_KEY] as string
      } catch {
        token = undefined
      }
    }
    const authToken = new AuthToken(token);
    await authToken.setup();
    return authToken;
  }
}