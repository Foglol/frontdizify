import { Dropdown, List, Menu, Modal } from 'antd';
import React, { useEffect } from 'react';
import { Title } from '../common/interfaces/title.interface';
import { MoreOutlined, UnorderedListOutlined } from '@ant-design/icons';

import { inject, observer } from 'mobx-react';
import { UserStore } from '../common/stores/user.store';
import { Playlist } from '../common/interfaces/playlist.interface';
import { HeartFilled, HeartOutlined }from '@ant-design/icons';
import { UserLevel } from '../common/enums/user-level.enum';

interface IProps {
  title: Title,
  userStore?: UserStore
}

const TitleListItem: React.FC<IProps> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore
}))(observer((props: IProps) => {

  function getTitleTime(title: Title): string {
    const seconds = title.duration;
    const remainingSeconds = seconds % 60;
    const minutes = (seconds - remainingSeconds) / 60;
    if (remainingSeconds < 10) {
      return `${minutes}:0${remainingSeconds}`;
    } else {
      return `${minutes}:${remainingSeconds}`;
    }
  }

  function handleAddToPlaylist(playlist: Playlist, title: Title) {
    props.userStore.addTitleToPlaylist(playlist, title);
  }

  function handleRemoveFromPlaylist(playlist: Playlist, title: Title) {
    props.userStore.removeTitleFromPlaylist(playlist, title);
  }

  function getPlaylistWhereTitleIs(title: Title) {
    return props.userStore.playlists.filter((playlist: Playlist) => (
      playlist.titles.find((t: Title) => (title.id === t.id))
    ))
  }

  function getPlaylistWhereTitleisNot(title: Title) {
    return props.userStore.playlists.filter((playlist: Playlist) => (
      !playlist.titles.find((t: Title) => (title.id === t.id))
    ))
  }

  function isInFavorites () {
    let title = props.userStore.favorite?.titles.find(e => e.id === props.title.id);
    return !!title
  }

  return (
    <List.Item className="title-list-item">
      <List.Item.Meta
        title={props.title.name}
      />
      <div className="title-item-end-content">
        <p>{getTitleTime(props.title)}</p>
        {props.userStore.authToken?.level === UserLevel.User && <div>
          {isInFavorites()
            ? <HeartFilled style={ {color: "#FF69B4", marginLeft: "10px" } }  title="Retirer des favoris" onClick={() => props.userStore.removeTitleFromFavorites(props.title)}/>
            : <HeartOutlined style={ {color: "#FF69B4", marginLeft: "10px"} } title="Ajouter aux favoris" onClick={() => props.userStore.addTitleToFavorites(props.title)}/>
          }
          </div>
        }
        {props.userStore.authToken?.level === UserLevel.User && <Dropdown overlay={
          <Menu>

            {/* Remove from playlist item group */}
            <Menu.ItemGroup title="Retirer de la playlist">
              {getPlaylistWhereTitleIs(props.title).map(playlist => (
                <Menu.Item>
                  <p onClick={() => {handleRemoveFromPlaylist(playlist, props.title)}}>
                    {playlist.id}
                  </p>
                </Menu.Item>
              ))}
            </Menu.ItemGroup>

            {/* Add to playlist item group */}
            <Menu.ItemGroup title="Ajouter à la playlist">
              {getPlaylistWhereTitleisNot(props.title).map((playlist) => (
                <Menu.Item>
                  <p onClick={() => {handleAddToPlaylist(playlist, props.title)}}>
                    {playlist.id}
                  </p>
                </Menu.Item>
              ))}
            </Menu.ItemGroup>

          </Menu>
        }>
          <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
            <UnorderedListOutlined className="list-item-menu-icon" />
          </a>
        </Dropdown>}
      </div>
      <Modal
        title="Ajouter ce titre à une playlist"
        visible={false}
        >
        
      </Modal>
    </List.Item>
    
  )
}))

export default TitleListItem;