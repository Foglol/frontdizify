import React, { useEffect, useState } from 'react';
import {Layout, Menu, Breadcrumb, Button, notification} from 'antd';
import { UserOutlined, LaptopOutlined, NotificationOutlined, CustomerServiceOutlined, HomeOutlined, HeartOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { AuthToken } from '../common/services/auth-token.service';
import { UserStore } from '../common/stores/user.store';
import { PlaylistsService } from '../common/services/playlists.service';
import { inject, observer } from 'mobx-react';
import { UserLevel } from '../common/enums/user-level.enum';
import { FavoritesService } from '../common/services/favorites.service';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

interface Props {
  children: React.ReactNode,
  userStore?: UserStore,
  authToken?: AuthToken,
}


export const BaseLayout: React.FC<Props> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore
}))(observer(

  (props: Props) => {

    const router = useRouter();
  
    useEffect(() => {
      fetchUserPlaylists();
      fetchFavoriteForCurrentUser();
    })
  
    async function fetchUserPlaylists() {
      const playlists = await PlaylistsService.getInstance().getAll(props.authToken.token);
      props.userStore.playlists = playlists;
    }

    async function fetchFavoriteForCurrentUser() {
      const favorite = await FavoritesService.getInstance().getFavoritesForCurrentUser(props.authToken.token);
      props.userStore.favorite = favorite
    }
  
    /**
     * Get the selected menu on page refresh
     * @return {string} the selecred menu key
     */
    function getSelectedMenuKey(): string {
      const routeRoot = router.pathname.split('/')[1]
      switch(routeRoot) {
        case 'albums': return '2';
        case 'artists': return '3';
        case 'playlists': return '4';
        case 'favorites': return '5';
        default: return '1';
      }
    }
  
    /**
     * Get the selected menu on page refresh
     * @return {string} the selecred menu key
     */
    function getSelectedHeaderMenuKey(): string {
      const routeRoot = router.pathname.split('/')[1]
      switch(routeRoot) {
        case 'login': return '1';
        case 'registration': return '2';
        default: return '1';
      }
    }

    async function logout() {
      await AuthToken.removeToken()
      props.userStore.authToken = null;
      router.push("/")
      notification.success({
        message: "Vous avez été déconnecté"
      })
    }
  
    return (
      <Layout className="layout" style={{ minHeight: '100vh' }}>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
          <div className="logo" />
          <Menu 
            theme="dark" 
            mode="horizontal" 
            defaultSelectedKeys={[getSelectedHeaderMenuKey()]}>
            {!props.authToken?.token && <Menu.Item key="1" ><Link href='/login'>Se connecter</Link></Menu.Item>}
            {!props.authToken?.token && <Menu.Item key="2" ><Link href='/registration'>Créer son compte</Link></Menu.Item>}
            {props.authToken?.token && props.authToken?.level === UserLevel.User && <Menu.Item key="3" ><Link href='/user'>Gérer votre compte</Link></Menu.Item>}
            {props.authToken?.token && <Menu.Item key="4" onClick={logout}>Déconnexion</Menu.Item>}
            {props.authToken?.token && props.authToken?.level === UserLevel.Admin && <Menu.Item key="5"><Link href='/admin'>Admin</Link></Menu.Item>}
          </Menu>
        </Header>
  
        <Layout style={{ marginLeft: 200, marginTop: 64, minHeight: 'calc(100% - 64px)' }}>
          <Sider width={200} className="site-layout-background" style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
          }}>
            <Menu
              mode="inline"
              defaultSelectedKeys={[getSelectedMenuKey()]}
              // defaultOpenKeys={['sub1']}
              style={{ height: '100%', borderRight: 0 }}
            >
              <Menu.Item key="1" icon={<HomeOutlined />}><Link href='/'>Home</Link></Menu.Item>
              <Menu.Item key="2" icon={<CustomerServiceOutlined />}><Link href='/albums'>Albums</Link></Menu.Item>
              <Menu.Item key="3" icon={<UserOutlined />}><Link href='/artists'>Artistes</Link></Menu.Item>
              {props.authToken?.token && props.authToken?.level === UserLevel.User && <Menu.Item key="4" icon={<UserOutlined />}><Link href='/playlists'>Playlists</Link></Menu.Item>}
              {props.authToken?.token && props.authToken?.level === UserLevel.User && <Menu.Item key="5" icon={<HeartOutlined />}><Link href='/favorites'>Favoris</Link></Menu.Item>}
            </Menu>
          </Sider>
  
          <Layout style={{ padding: '0 24px 24px', minHeight: '100%' }}>
            <Content
              className="site-layout-background"
              style={{
                padding: 24,
                margin: "24px 0 0 0",
                minHeight: 280,
              }}
            >
              {props.children}
            </Content>
            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
  
          </Layout>
        </Layout>
        
      </Layout>
    );
  }
))

