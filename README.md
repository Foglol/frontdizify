This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Project architecture

```javascript
src
-- components
-- common
---- interfaces // all interface files
---- constants // all files exporting constants
---- enums // all enums files
---- services // Should contain some Singleton class to call web services
---- utils // all utils files.
pages
styles
```

### Files naming convention
- **interface** `[interfacename].interface.ts` user.interface.ts
- **constants** `[constantsname].constants.ts` apiErrorMessages.constants.ts
- **enums** `[enumname].enum.ts` role.enum.ts
- **services** `[servicename].service.ts` user.service.ts
- **utils** `[utilsname].utils.ts` string.utils.ts
- **components** `[componentname].component.tsx` header.component.tsx


## Pages and routing

Routing is automatically generated. The `pages` folder manage the urls.
Name your page like the end of the route, or store an `index.tsx` file into a named folder.

examples :
```javascript
pages
--index.tsx // --> /
--login.tsx // --> /login
--albums
----index.tsx // --> /albums
----[albumId]
------index.tsx // --> /albums/{albumid}
```

## Environement variable

```
NEXT_PUBLIC_API_URL #The api URL Exemple http://dizify.bartradingx.com/api
```
